/*!
 * @file ethernet.grove.ino
 *
 * @section intro Introduction
 *
 * Ce programme va utiliser le Grove Ethernet afin d’héberger et de rendre accessible une page web qui aura comme contenu les informations du capteur BME 280.
 * Cette page web permettra aussi de manipuler une LED à distance avec l’aide de requête GET.
 *
 * @section libraries Librairies
 * 
 * Ce fichier dépend des librairies suivantes :\n\n
 * 
 * BME 280 : Grove_BME280-master.zip
 * 
 * Chainable LED : ChainableLED-master.zip
 *
 * @section author Auteur
 *
 * Réalisé par Hugo PERVEYRIE
 *
 */
 
#include "SPI.h"
#include "Ethernet.h"
#include "Seeed_BME280.h"
#include <ChainableLED.h>

#define NUM_LEDS  1 ///< Nombre de LED de la Chainable LED
#define LED21_PIN 6 ///< PIN de la Chainable LED en entrée
#define LED22_PIN 7 ///< PIN de la Chainable LED en sortie

byte mac[]= { 0x90, 0xA2, 0xDA , 0x0F, 0xF9, 0xFD}; ///< Adresse MAC du Grove Ethernet
IPAddress ip(192,168,1,253); ///< Adresse IP du Grove Ethernet
EthernetServer myserver(80); ///< Serveur herbergé sur le port 80
BME280 bme280; ///< Le baromètre
ChainableLED leds(LED21_PIN, LED22_PIN, NUM_LEDS); ///< La chainable LED

/*!
 @brief Cette fonction est éxécuter 1 seule fois avant la fonction loop(). Elle initialise la Chainable LED, le baromètre et le serveur web

 @param Pas de paramètre

 @return Pas de retour

 @code
void setup() {
  Serial.begin(9600);
  
  // Initialisation du Grove Ethernet, du baromètre et de la led
  Ethernet.begin(mac, ip);
  myserver.begin();
  bme280.init();
  leds.init();

  // On initialise la led en blanc
  for (byte i=0; i<NUM_LEDS; i++){
      leds.setColorRGB(i, 255, 255, 255);
    }
}
 @endcode
*/
void setup() {
  Serial.begin(9600);
  
  // Initialisation du Grove Ethernet, du baromètre et de la led
  Ethernet.begin(mac, ip);
  myserver.begin();
  bme280.init();
  leds.init();

  // On initialise la led en blanc
  for (byte i=0; i<NUM_LEDS; i++){
      leds.setColorRGB(i, 255, 255, 255);
    }
}


/*!
 @brief Cette fonction sera éxécuté en boucle par le controlleur. Elle affiche la température sur le serveur web et récupère les valeurs analogiques RGB du serveur grâce à une requête GET pour les affecter à la Chainable LED.

 @param Pas de paramètre

 @return Pas de retour

 @code
void loop() {

  // On récupère les valeurs du module BME 280
  float temperature = bme280.getTemperature();
  float humidite = bme280.getHumidity();
  EthernetClient client = myserver.available();
  String reqString = "";
  
  if (client) {

    // On lit les caractères de la requête un par un pour les ajouter dans un String
    char c = "";   
    while (c != '\n') {
      c = client.read();
      reqString += c;
      Serial.print(c);
    }

    // reqString contient la requête GET

    // Debut du header
    client.println("HTTP/1.1 200 OK");
    client.println("Content-Type: text/html");
    client.println();
    // Fin du Header


    // Définition du document
    client.println("<!DOCTYPE html>");
    client.println("<html>");
    client.println("<head>");
    client.println("<meta http-equiv=\"refresh\" content=\"15\">");
    client.println("</head>");
    client.println("<body>");

    // Affichage du barometre
    client.println("<h2>BME 280</h2>");
    
    client.print("Temperature : ");
    client.print(temperature);
    client.print("<br>");
    client.print("Humidite : ");
    client.print(humidite);
    client.print("<br>");

    client.println("<br>");

    // Pilotage de la LED
    client.println("<h2>Chainable LED</h2>");
    client.print("<br>");

    // Décalage des valeurs de 100 pour des raisons de parsing
    client.println("Rouge : ")
    client.println("<input type=\"range\" id=\"r\" name=\"rouge\" min=\"100\" max=\"355\" value=\"100\" autocomplete=\"on\">");
    client.println("Vert : ");
    client.println("<input type=\"range\" id=\"g\" name=\"vert\" min=\"100\" max=\"355\" value=\"100\" autocomplete=\"on\">");
    client.println("Bleu : ");
    client.println("<input type=\"range\" id=\"b\" name=\"bleu\" min=\"100\" max=\"355\" value=\"100\" autocomplete=\"on\">");
    client.println("<br>");

    client.println("<button id=\"color\">Changer teinte</button>");
    client.println("<br>");

    // Script Javascript qui va effectuer une requête GET lorsque l'utilisateur change la couleur avec les valeurs correspondantes
    client.println("<script type=\"text/javascript\"> let rouge = document.getElementById('r'); let vert= document.getElementById('g'); let bleu = document.getElementById('b');document.getElementById('color').onclick = function(){location.href = \"http://192.168.1.253/?color=\" + rouge.value + \"-\" + vert.value + \"-\" + bleu.value;};</script>");
    client.println("</body>");
    client.println("</html>");
  }
  
  delay(10);
  client.stop();

  // On recupere le debut de la requête color
  int paramGet = reqString.indexOf("?color=") + 7;

  // Si la requête GET color a été effectué
  if(paramGet != 6) {

    // On récupère les couleurs avec du parsing en faisant attention à enlever 100
    int rouge = reqString.substring(paramGet,paramGet+3).toInt() - 100;
    int vert = reqString.substring(paramGet+4,paramGet+7).toInt() - 100;
    int bleu = reqString.substring(paramGet+8,paramGet+11).toInt() - 100;

    // On affiche la couleur sur la led
    for (byte i=0; i<NUM_LEDS; i++){
      leds.setColorRGB(i, rouge, vert, bleu);
    }
  }
}
 @endcode
*/
void loop() {

  // On récupère les valeurs du module BME 280
  float temperature = bme280.getTemperature();
  float humidite = bme280.getHumidity();
  EthernetClient client = myserver.available();
  String reqString = "";
  
  if (client) {

    // On lit les caractères de la requête un par un pour les ajouter dans un String
    char c = "";   
    while (c != '\n') {
      c = client.read();
      reqString += c;
      Serial.print(c);
    }

    // reqString contient la requête GET

    // Debut du header
    client.println("HTTP/1.1 200 OK");
    client.println("Content-Type: text/html");
    client.println();
    // Fin du Header


    // Définition du document
    client.println("<!DOCTYPE html>");
    client.println("<html>");
    client.println("<head>");
    client.println("<meta http-equiv=\"refresh\" content=\"15\">");
    client.println("</head>");
    client.println("<body>");

    // Affichage du barometre
    client.println("<h2>BME 280</h2>");
    
    client.print("Temperature : ");
    client.print(temperature);
    client.print("<br>");
    client.print("Humidite : ");
    client.print(humidite);
    client.print("<br>");

    client.println("<br>");

    // Pilotage de la LED
    client.println("<h2>Chainable LED</h2>");
    client.print("<br>");

    // Décalage des valeurs de 100 pour des raisons de parsing
    client.println("Rouge : ")
    client.println("<input type=\"range\" id=\"r\" name=\"rouge\" min=\"100\" max=\"355\" value=\"100\" autocomplete=\"on\">");
    client.println("Vert : ");
    client.println("<input type=\"range\" id=\"g\" name=\"vert\" min=\"100\" max=\"355\" value=\"100\" autocomplete=\"on\">");
    client.println("Bleu : ");
    client.println("<input type=\"range\" id=\"b\" name=\"bleu\" min=\"100\" max=\"355\" value=\"100\" autocomplete=\"on\">");
    client.println("<br>");

    client.println("<button id=\"color\">Changer teinte</button>");
    client.println("<br>");

    // Script Javascript qui va effectuer une requête GET lorsque l'utilisateur change la couleur avec les valeurs correspondantes
    client.println("<script type=\"text/javascript\"> let rouge = document.getElementById('r'); let vert= document.getElementById('g'); let bleu = document.getElementById('b');document.getElementById('color').onclick = function(){location.href = \"http://192.168.1.253/?color=\" + rouge.value + \"-\" + vert.value + \"-\" + bleu.value;};</script>");
    client.println("</body>");
    client.println("</html>");
  }
  
  delay(10);
  client.stop();

  // On recupere le debut de la requête color
  int paramGet = reqString.indexOf("?color=") + 7;

  // Si la requête GET color a été effectué
  if(paramGet != 6) {

    // On récupère les couleurs avec du parsing en faisant attention à enlever 100
    int rouge = reqString.substring(paramGet,paramGet+3).toInt() - 100;
    int vert = reqString.substring(paramGet+4,paramGet+7).toInt() - 100;
    int bleu = reqString.substring(paramGet+8,paramGet+11).toInt() - 100;

    // On affiche la couleur sur la led
    for (byte i=0; i<NUM_LEDS; i++){
      leds.setColorRGB(i, rouge, vert, bleu);
    }
  }
}