/*!
 * @file lcd.temperature.ino
 *
 * @section intro Introduction
 *
 * Ce programme va utiliser le capteur BME 280 afin de récupérer les valeurs de température et d’humidité et de les afficher sur un affichage LCD. 
 * En plus de cette partie, un potentiomètre va permettre de piloter l’allumage d’une LED. 
 * En effet après avoir dépassé une certaine valeur analogique que l’on appellera seuil, la LED s’allumera ou s’éteindra.
 *
 * @section libraries Librairies
 * 
 * Ce fichier dépend des librairies suivantes :\n\n
 * 
 * BME 280 : Grove_BME280-master.zip
 * 
 * Chainable LED : ChainableLED-master.zip
 *
 * Ecran LCD : Grove_LCD_RGB_Backlight-master.zip
 *
 * @section author Auteur
 *
 * Réalisé par Hugo PERVEYRIE
 *
 */

#include <ChainableLED.h>
#include <Wire.h>
#include "rgb_lcd.h"
#include "Seeed_BME280.h"

#define NUM_LEDS  1 ///< Nombre de LED de la Chainable LED
#define LED1_PIN 2 ///< PIN de la LED basique
#define PWM_PIN 3 ///< PIN du potentiomètre
#define LED21_PIN 6 ///< PIN de la Chainable LED en entrée
#define LED22_PIN 7 ///< PIN de la Chainable LED en sortie

rgb_lcd lcd; ///< L'écran LCD
BME280 bme280; ///< Le baromètre
ChainableLED leds(LED21_PIN, LED22_PIN, NUM_LEDS); ///< La chainable LED
int val = 0; ///< Valeur Analogique pour le potentiomètre

/*!
 @brief Cette fonction est éxécuter 1 seule fois avant la fonction loop(). Elle initialise l’écran LCD, la Chainable LED et la LED basique.

 @param Pas de paramètre

 @return Pas de retour

 @code
void setup()
{
   // Définit les colonnes et lignes du l'affichage LCD
  lcd.begin(16, 2);

  // Initialisation du baromètre
  if(!bme280.init()){
    lcd.setRGB(255, 0, 0);
    lcd.print("Erreur !");
  }else{
    lcd.setRGB(0, 255, 0);
    lcd.print("Parfait !");
  }

  // Initialisation de la LED RGB
  leds.init();

  // Initialise l'écran en blanc
  delay(1000);
  lcd.setRGB(255, 255, 255);

  // Definit le port LED1_PIN en mode sortie
  pinMode(LED1_PIN, OUTPUT);

  // Attribue le bit O au port LED1_PIN
  digitalWrite(LED1_PIN, LOW);
}
 @endcode
*/
void setup()
{
   // Définit les colonnes et lignes du l'affichage LCD
  lcd.begin(16, 2);

  // Initialisation du baromètre
  if(!bme280.init()){
    lcd.setRGB(255, 0, 0);
    lcd.print("Erreur !");
  }else{
    lcd.setRGB(0, 255, 0);
    lcd.print("Parfait !");
  }

  // Initialisation de la LED RGB
  leds.init();

  // Initialise l'écran en blanc
  delay(1000);
  lcd.setRGB(255, 255, 255);

  // Definit le port LED1_PIN en mode sortie
  pinMode(LED1_PIN, OUTPUT);

  // Attribue le bit O au port LED1_PIN
  digitalWrite(LED1_PIN, LOW);
}

/*!
 @brief Cette fonction sera éxécuté en boucle par le controlleur. Elle affiche les valeurs du capteur BME 280 sur l’écran LCD en déplaçant le curseur sur les différentes lignes de l’écran. 
 Elle affecte ensuite à la Chainable LED une couleur différente en fonction de la température actuel. 
 Pour terminer elle va lire la valeur du potentiomètre et si cette valeur analogique dépasse le seuil de 511, la LED va alors être allumé. Sinon elle sera éteinte.

 @param Pas de paramètre

 @return Pas de retour

 @code
void loop()
{
  // Affichage de la température
  lcd.setCursor(0,0);
  lcd.print("Degres   : ");
  lcd.setCursor(11,0);
  float temp = bme280.getTemperature();
  lcd.print(temp);

  // Affichage de l'humidité
  lcd.setCursor(0,1);
  lcd.print("Humidity : ");
  lcd.setCursor(11,1);
  lcd.print(bme280.getHumidity());
  //

  // Change la couleur de la led en fonction de la température
  for (byte i=0; i<NUM_LEDS; i++){

    if(temp < 10.0){
      leds.setColorRGB(i, 0, 0, 255);
    }else{
      if((temp>=10.0) && (temp < 22)){
        leds.setColorRGB(i, 255, 255, 0);
      }else{
        leds.setColorRGB(i, 255, 0, 0);
      }
    }
  }

  // Recupere la valeur de l'entree analogue
  val = analogRead(PWM_PIN);

  if (val >= 511) {
    // Attribue le bit 1 au port LED1_PIN
    digitalWrite(LED1_PIN, HIGH);
  }
  else {
    // Attribue le bit 0 au port LED1_PIN
    digitalWrite(LED1_PIN, LOW);
  }

  delay(100);

  lcd.clear();
}
 @endcode
*/  
void loop()
{
  // Affichage de la température
  lcd.setCursor(0,0);
  lcd.print("Degres   : ");
  lcd.setCursor(11,0);
  float temp = bme280.getTemperature();
  lcd.print(temp);

  // Affichage de l'humidité
  lcd.setCursor(0,1);
  lcd.print("Humidity : ");
  lcd.setCursor(11,1);
  lcd.print(bme280.getHumidity());
  //

  // Change la couleur de la led en fonction de la température
  for (byte i=0; i<NUM_LEDS; i++){

    if(temp < 10.0){
      leds.setColorRGB(i, 0, 0, 255);
    }else{
      if((temp>=10.0) && (temp < 22)){
        leds.setColorRGB(i, 255, 255, 0);
      }else{
        leds.setColorRGB(i, 255, 0, 0);
      }
    }
  }

  // Recupere la valeur de l'entree analogue
  val = analogRead(PWM_PIN);

  if (val >= 511) {
    // Attribue le bit 1 au port LED1_PIN
    digitalWrite(LED1_PIN, HIGH);
  }
  else {
    // Attribue le bit 0 au port LED1_PIN
    digitalWrite(LED1_PIN, LOW);
  }

  delay(100);

  lcd.clear();
}
