/*!
 * @file lcd.temperature.without.lib.ino
 *
 * @section intro Introduction
 *
 * Ce programme va utiliser le capteur BME 280 afin de récupérer les valeurs de température et d’humidité et de les afficher sur un affichage LCD. 
 * Contrairement au programme précédent le capteur BME 280 sera utilisé sans sa librairie.
 *
 * @section libraries Librairies
 * 
 * Ce fichier dépend des librairies suivantes :\n\n
 * 
 * Chainable LED : ChainableLED-master.zip
 *
 * Ecran LCD : Grove_LCD_RGB_Backlight-master.zip
 *
 * @section author Auteur
 *
 * Réalisé par Hugo PERVEYRIE
 *
 */
 
#include <Arduino.h>
#include <Wire.h>
#include <ChainableLED.h>
#include "rgb_lcd.h"

#define NUM_LEDS  1 ///< Nombre de LED de la Chainable LED
#define BME280_ADDRESS   0x76 ///< Adresse du capteur BME 280
#define BME280_REG_TEMPDATA 0xFA ///< Valeur du registre pour la température
#define BME280_REG_DIG_T1    0x88 ///< Valeur du registre pour dig_T1
#define BME280_REG_DIG_T2    0x8A ///< Valeur du registre pour dig_T2
#define BME280_REG_DIG_T3    0x8C ///< Valeur du registre pour dig_T3
#define LED21_PIN 6 ///< PIN de la Chainable LED en entrée
#define LED22_PIN 7 ///< PIN de la Chainable LED en sortie


uint16_t dig_T1; ///< Valeur 1 du capteur BME 280
int16_t dig_T2; ///< Valeur 2 du capteur BME 280
int16_t dig_T3; ///< Valeur 3 du capteur BME 280
rgb_lcd lcd; ///< L'écran LCD
ChainableLED leds(LED21_PIN, LED22_PIN, NUM_LEDS); ///< La chainable LED


/*!
 @brief Cette fonction formate les valeurs du capteur pour qu'elle corresponde à une température en degré.

 @param Pas de paramètre

 @return Retourne la température en degré

 @code
float getTemperature(void) {
    int32_t var1, var2;

    // Récupère la valeur venant du capteur
    int32_t adc_T = BME280Read24(BME280_REG_TEMPDATA);
    
    // Formate la valeur du capteur pour la transformer en température
    adc_T >>= 4;
    var1 = (((adc_T >> 3) - ((int32_t)(dig_T1 << 1))) *
            ((int32_t)dig_T2)) >> 11;

    var2 = (((((adc_T >> 4) - ((int32_t)dig_T1)) *
              ((adc_T >> 4) - ((int32_t)dig_T1))) >> 12) *
            ((int32_t)dig_T3)) >> 14;

    int32_t t_fine = var1 + var2;
    float T = (t_fine * 5 + 128) >> 8;

    return T / 100;
}
 @endcode
*/
float getTemperature(void) {
    int32_t var1, var2;

    // Récupère la valeur venant du capteur
    int32_t adc_T = getValue(BME280_REG_TEMPDATA);
    
    // Formate la valeur du capteur pour la transformer en température
    adc_T >>= 4;
    var1 = (((adc_T >> 3) - ((int32_t)(dig_T1 << 1))) *
            ((int32_t)dig_T2)) >> 11;

    var2 = (((((adc_T >> 4) - ((int32_t)dig_T1)) *
              ((adc_T >> 4) - ((int32_t)dig_T1))) >> 12) *
            ((int32_t)dig_T3)) >> 14;

    int32_t t_fine = var1 + var2;
    float T = (t_fine * 5 + 128) >> 8;

    return T / 100;
}


/*!
 @brief Cette fonction initialise le capteur BME 280

 @param Pas de paramètre

 @return Retourne un booléen de la réussite de l'initialisation

 @code
bool initTemperature() {
    // Se connecte en tant que maître
    Wire.begin();

    uint16_t data1 = getValue16(BME280_REG_DIG_T1);
    dig_T1 = (data1 >> 8) | (data1 << 8);

    uint16_t data2 = getValue16(BME280_REG_DIG_T2);
    dig_T2 = (int16_t) (data2 >> 8) | (data2 << 8);

    uint16_t data3 = getValue16(BME280_REG_DIG_T3);
    dig_T3 = (int16_t) ((data3 >> 8) | (data3 << 8));

    return true;
}
 @endcode
*/
bool initTemperature() {
    // Se connecte en tant que maître
    Wire.begin();

    uint16_t data1 = getValue16(BME280_REG_DIG_T1);
    dig_T1 = (data1 >> 8) | (data1 << 8);

    uint16_t data2 = getValue16(BME280_REG_DIG_T2);
    dig_T2 = (int16_t) (data2 >> 8) | (data2 << 8);

    uint16_t data3 = getValue16(BME280_REG_DIG_T3);
    dig_T3 = (int16_t) ((data3 >> 8) | (data3 << 8));

    return true;
}


/*!
 @brief Cette fonction récupère des valeurs du capteur BME280

 @param uint8_t reg La valeur de registre permettant de communiquer avec le capteur

 @return uint16_t La donnée du capteur

 @code
uint16_t getValue16(uint8_t reg) {
    uint8_t msb, lsb;
    
    // Demande au capteur l'envoi de la valeur
    Wire.beginTransmission(BME280_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();

    // Récupère la valeur
    Wire.requestFrom(BME280_ADDRESS, 2);
    msb = Wire.read();
    lsb = Wire.read();

    return (uint16_t) msb << 8 | lsb;
}
 @endcode
*/
uint16_t getValue16(uint8_t reg) {
    uint8_t msb, lsb;
    
    // Demande au capteur l'envoi de la valeur
    Wire.beginTransmission(BME280_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();

    // Récupère la valeur
    Wire.requestFrom(BME280_ADDRESS, 2);
    msb = Wire.read();
    lsb = Wire.read();

    return (uint16_t) msb << 8 | lsb;
}


/*!
 @brief Cette fonction retourne une valeur du capteur

 @param uint8_t reg La valeur de registre permettant de communiquer avec le capteur 

 @return uint32_t La donnée du capteur

 @code
uint32_t getValue(uint8_t reg) {
    uint32_t data;

    // Demande au capteur l'envoi de la valeur
    Wire.beginTransmission(BME280_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();

    // Récupère la valeur
    Wire.requestFrom(BME280_ADDRESS, 3);
    data = Wire.read();
    data <<= 8;
    data |= Wire.read();
    data <<= 8;
    data |= Wire.read();

    return data;
}
 @endcode
*/
uint32_t getValue(uint8_t reg) {
    uint32_t data;

    // Demande au capteur l'envoi de la valeur
    Wire.beginTransmission(BME280_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();

    // Récupère la valeur
    Wire.requestFrom(BME280_ADDRESS, 3);    
    data = Wire.read();
    data <<= 8;
    data |= Wire.read();
    data <<= 8;
    data |= Wire.read();

    return data;
}


/*!
 @brief Cette fonction est éxécuter 1 seule fois avant la fonction loop(). Elle initialise l'écran LCD, la Chainable LED et le capteur BME280

 @param Pas de paramètre

 @return Pas de retour

 @code
void setup() {
  // Définit les colonnes et lignes du l'affichage LCD
  lcd.begin(16, 2);

  // Initialisation de la LED RGB
  leds.init();

  // Initialisation du baromètre
  if(!initTemperature()){
    lcd.setRGB(255, 0, 0);
    lcd.print("Erreur !");
  }else{
    lcd.setRGB(0, 255, 0);
    lcd.print("Parfait !");
  }

  // Initialise l'écran en blanc
  delay(1000);
  lcd.setRGB(255, 255, 255); 
}
 @endcode
*/
void setup() {
  // Définit les colonnes et lignes du l'affichage LCD
  lcd.begin(16, 2);

  // Initialisation de la LED RGB
  leds.init();

  // Initialisation du baromètre

  if(!initTemperature()){
    lcd.setRGB(255, 0, 0);
    lcd.print("Erreur !");
  }else{
    lcd.setRGB(0, 255, 0);
    lcd.print("Parfait !");
  }

  // Initialise l'écran en blanc
  delay(1000);
  lcd.setRGB(255, 255, 255); 
}


/*!
 @brief Cette fonction sera éxécuté en boucle par le controlleur. Elle affiche la température sur l'écran LCD.

 @param Pas de paramètre

 @return Pas de retour

 @code
void loop() {
  // Affichage de la température
  lcd.setCursor(0,0);
  float temp = getTemperature();
  lcd.print("Degres : ");
  lcd.print(temp);
  Serial.println(temp);
  Serial.println("test");
  delay(500);

  // Change la couleur de la led en fonction de la température
  for (byte i=0; i<NUM_LEDS; i++){

    if(temp < 10.0){
      leds.setColorRGB(i, 0, 0, 255);
    }else{
      if((temp>=10.0) && (temp < 22)){
        leds.setColorRGB(i, 255, 255, 0);
      }else{
        leds.setColorRGB(i, 255, 0, 0);
      }
    }
  }
}
 @endcode
*/
void loop() {
  // Affichage de la température
  lcd.setCursor(0,0);
  float temp = getTemperature();
  lcd.print("Degres : ");
  lcd.print(temp);
  Serial.println(temp);
  Serial.println("test");
  delay(500);

  // Change la couleur de la led en fonction de la température
  for (byte i=0; i<NUM_LEDS; i++){

    if(temp < 10.0){
      leds.setColorRGB(i, 0, 0, 255);
    }else{
      if((temp>=10.0) && (temp < 22)){
        leds.setColorRGB(i, 255, 255, 0);
      }else{
        leds.setColorRGB(i, 255, 0, 0);
      }
    }
  }
}
