var indexSectionsWithContent =
{
  0: "bdegilmnpsv",
  1: "elm",
  2: "gilms",
  3: "bdlmv",
  4: "blnp",
  5: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "defines",
  5: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Fichiers",
  2: "Fonctions",
  3: "Variables",
  4: "Macros",
  5: "Pages"
};

